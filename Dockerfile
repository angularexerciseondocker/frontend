# building (docker image is built with docker compose in yml file)
FROM node:17.9.1-alpine AS build
WORKDIR /app
COPY package*.json ./
RUN npm install -g @angular/cli@16.2.0
COPY . .
RUN npm install
RUN npm run build

# production:
FROM nginx:alpine
COPY --from=build /app/dist/angular-on-docker /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 4200
CMD ["nginx", "-g", "daemon off;"]
