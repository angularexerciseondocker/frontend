import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { iData } from '../components/file-upload-xlsx/file-upload-xlsx.component';

@Injectable({
  providedIn: 'root'
})
export class FiledataService {

  url = "http://localhost:8081";

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private httpClient: HttpClient) {}

  getAllFilesData():Observable<any>{
    const url = `${this.url}/api/filedata`;
    return this.httpClient.get<any>(url);
  }

  //Add one record at a time:
  addData(storedData: iData):Observable<any>{
    const requestBody = JSON.stringify(storedData).split('\\"').join("");
    const url = `${this.url}/api/filedata`;
    return this.httpClient.post<any>(url, requestBody, this.httpOptions);
  }  

  //Add multiple records at once:
  addDataRecords(storedDataRecords: iData[]):Observable<iData[]>{
    const requestBody = JSON.stringify(storedDataRecords);
    const url = `${this.url}/api/filedata/records`;
    return this.httpClient.post<any>(url, requestBody, this.httpOptions);
  }  
}
