import { Component, OnInit } from '@angular/core';
import { Chart, ChartConfiguration, registerables } from 'chart.js';
import annotationPlugin, { AnnotationOptions, AnnotationTypeRegistry } from 'chartjs-plugin-annotation';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/services/data.service';
Chart.register(annotationPlugin);
Chart.register(...registerables);

@Component({
  selector: 'app-flowchart',
  templateUrl: './flowchart.component.html',
  styleUrls: ['./flowchart.component.css']
})
export class FlowchartComponent implements OnInit {
  chart!: Chart;
  element: any;
  lastEvent: any;
  isUpdating: boolean = false;
  theCanvas!: any;
  rightBorder!: number;
  leftBorder!: number;
  chartMaxX: number = 100;
  chartMaxY: number = 100;

  subscription!: Subscription;
  dataRow: string[] = [];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.getDataFromTable();
    this.getDataFromLocalStorage();
    this.setUpChart();
  }


  /** empty chart data when returning back to table view */
  emptyChartData() {
    if (localStorage.getItem('dataRow') !== null) {
      localStorage.removeItem('dataRow');
    }
  }

  /** Gets data from table component over data service:*/
  getDataFromTable() {
    this.dataService.dataArray$.subscribe(result => {
      this.dataRow = result;
    });
  }

  saveToLocalStorage() {
    localStorage['dataRow'] = JSON.stringify(this.dataRow);
  }

  /** to display data after the page refresh*/
  getDataFromLocalStorage() {
    if (localStorage.getItem('dataRow') !== null) {
      this.dataRow = JSON.parse(localStorage['dataRow']);
    }
    else {
      this.saveToLocalStorage();
    }
  }

  /**check if data was present in the table */
  isBusinessDictionaryTerm1(): boolean {
    return !!this.dataRow[3];
  }

  /** check if data was present in the table */
  isBusinessDictionaryTerm2(): boolean {
    return !!this.dataRow[4];
  }


  setUpChart(): void {

    /** array elements at index [x]; */
    const dataElementValues = this.dataRow[0];
    const descriptionLabelValue = this.dataRow[1];
    const dataDictionaryTermValue = this.dataRow[2];
    const businessDictionaryTerm1Value = this.dataRow[3];
    const businessDictionaryTerm2Value = this.dataRow[4];

    const annotDataElementValues: AnnotationOptions<keyof AnnotationTypeRegistry> = {
      id: 'box1',
      type: 'box',
      adjustScaleRange: true,
      backgroundColor: 'rgb(153, 204, 0, 1)',
      borderColor: 'rgb(0, 102, 0)',
      borderWidth: 3,
      borderRadius: 10,
      label: {
        display: true,
        content: [dataElementValues],
        textAlign: 'center',
        font: {
          size: 18
        }
      },
      xMin: 5,
      xMax: 25,
      yMin: 40,
      yMax: 60,
    };


    const annotDescriptionButton: AnnotationOptions<keyof AnnotationTypeRegistry> = {
      id: 'descriptionButton',
      type: 'box',
      adjustScaleRange: true,
      backgroundColor: 'white',
      borderColor: 'rgb(165, 214, 167)',
      borderWidth: 2,
      click: ({ element }) => this.showDescription(element),

      label: {
        display: true,
        content: ['?'],
        textAlign: 'center',
        font: {
          size: 23,
        }
      },
      xMin: 20,
      xMax: 24,
      yMin: 52,
      yMax: 58,
    };

    const annotDescriptionLabel: AnnotationOptions<keyof AnnotationTypeRegistry> = {
      id: 'descriptionLabel',
      type: 'label',
      display: false,
      backgroundColor: 'rgba(153, 153, 153, 0.25)',
      borderShadowColor: 'gray',
      shadowBlur: 3,
      shadowOffsetX: 2,
      shadowOffsetY: 2,
      content: [descriptionLabelValue],
      callout: {
        display: true,
        side: 10,
        margin: 0,
        position: 'left',
        start: '50%',
        borderDash: [2],
      },
      xValue: 24,
      yValue: 65,
      font: {
        size: 18
      },
      padding: 15,
    };

    const annotLineBtwDataElementAndDataTerm: AnnotationOptions<keyof AnnotationTypeRegistry> = {
      id: 'line1',
      type: 'line',
      adjustScaleRange: false,
      borderWidth: 3,
      borderColor: 'rgb(102, 51, 0)',
      xMin: 25,
      xMax: 30,
      yMin: 50,
      yMax: 50,
      arrowHeads: {
        end: {
          display: true,
        },
        width: 3,
      },
    };

    const annotDataDictionaryTermValue: AnnotationOptions<keyof AnnotationTypeRegistry> = {
      id: 'ellipse1',
      type: 'ellipse',
      xMin: 30,
      xMax: 70,
      yMin: 40,
      yMax: 60,
      label: {
        display: true,
        content: [dataDictionaryTermValue],
        textAlign: 'center',
        font: {
          size: 18
        }
      },
      backgroundColor: 'rgb(255, 153, 153, 1)',
      borderColor: 'rgb(204, 0, 0)',
      borderWidth: 3,
    };


    const annotLineBtwDataTermAndBusinessTerm1: AnnotationOptions<keyof AnnotationTypeRegistry> = {
      id: 'line2',
      type: 'line',
      display: this.isBusinessDictionaryTerm1(),
      adjustScaleRange: false,
      borderWidth: 3,
      borderColor: 'rgb(102, 51, 0)',
      xMin: 70,
      xMax: 75,
      yMin: 50,
      yMax: 70,
      arrowHeads: {
        end: {
          display: true,
        },
        width: 3,
      },
    };

    const annotLineBtwDataTermAndBusinessTerm2: AnnotationOptions<keyof AnnotationTypeRegistry> = {
      id: 'line3',
      type: 'line',
      display: this.isBusinessDictionaryTerm2(),
      adjustScaleRange: false,
      borderWidth: 3,
      borderColor: 'rgb(102, 51, 0)',
      xMin: 70,
      xMax: 75,
      yMin: 50,
      yMax: 30,
      arrowHeads: {
        end: {
          display: true,
        },
        width: 3,
      },
    };

    const annotBusinessDictionaryTerm1Value: AnnotationOptions<keyof AnnotationTypeRegistry> = {
      id: 'ellipse2',
      type: 'ellipse',
      display: this.isBusinessDictionaryTerm1(),
      xMin: 75,
      xMax: 95,
      yMin: 60,
      yMax: 80,
      label: {
        display: true,
        content: [businessDictionaryTerm1Value],
        textAlign: 'center',
        font: {
          size: 18
        }
      },
      backgroundColor: 'rgb(255, 204, 153, 1)',
      borderColor: 'rgb(155, 102, 0)',
      borderWidth: 3,
    };

    const annotBusinessDictionaryTerm2Value: AnnotationOptions<keyof AnnotationTypeRegistry> = {
      id: 'ellipse3',
      type: 'ellipse',
      display: this.isBusinessDictionaryTerm2(),
      xMin: 75,
      xMax: 95,
      yMin: 20,
      yMax: 40,
      label: {
        display: true,
        content: [businessDictionaryTerm2Value],
        textAlign: 'center',
        font: {
          size: 18,
        }
      },
      backgroundColor: 'rgb(255, 204, 153, 1)',
      borderColor: 'rgb(155, 102, 0)',
      borderWidth: 3,
    };

    const annotations = [
      annotDataElementValues,
      annotDescriptionButton,
      annotDescriptionLabel,
      annotLineBtwDataElementAndDataTerm,
      annotDataDictionaryTermValue,
      annotLineBtwDataTermAndBusinessTerm1,
      annotLineBtwDataTermAndBusinessTerm2,
      annotBusinessDictionaryTerm1Value,
      annotBusinessDictionaryTerm2Value,
    ];

    const data = {
      datasets: [
        {
          label: 'Dataset 1',
          data: [],
          backgroundColor: 'rgba(255, 99, 132, 0.5)',
          borderColor: 'rgba(255, 99, 132, 1)',
          borderWidth: 1
        },
      ]
    };


    const dragger = {
      id: 'dragger',
      beforeEvent: (chart: any, args: any, options: any) => {
        this.handleDrag(args.event);
        args.changed = true;
        return;
      }
    }

    const config: ChartConfiguration = {
      type: 'bubble',
      plugins: [dragger],
      data: data,
      options: {
        events: ['mousedown', 'mouseup', 'mousemove', 'mouseout'],
        scales: {
          y: {
            beginAtZero: true,
            display: false,
            min: 0,
            max: this.chartMaxY,
          },
          x: {
            beginAtZero: true,
            display: false,
            min: 0,
            max: this.chartMaxX,
          }
        },
        plugins: {
          legend: {
            display: false,
          },
          annotation: {
            enter: (ctx: any) => {
              this.element = ctx.element;
            },
            leave: () => {
              this.element = undefined;
              this.lastEvent = undefined;
            },
            annotations: annotations,
          }
        },
      }
    };


    const canvas = <HTMLCanvasElement>document.getElementById('myChart') as HTMLCanvasElement;
    const ctx = canvas.getContext('2d');
    canvas.style.backgroundColor = 'white';
    canvas.style.border = '1px solid #dee2e6';
    this.chart = new Chart(ctx!, config);

  }

  /** Check if the description of DataElement shape is visible */
  isDescriptionVisible: boolean = false;
  showDescription(element: any,) {
    if (element.$context.id == 'descriptionButton') {
      const annotations = this.chart?.options?.plugins?.annotation?.annotations as AnnotationOptions<any>[] | undefined;
      if (annotations) {
        annotations[2].display = !annotations[2].display;
        this.chart.update();
      }
    }
  }

  handleDrag(event: any): void {
    if (this.element) {
      switch (event.type) {
        case 'mousemove':
          this.handleElementDragging(event);
          break;
        case 'mouseout':
        case 'mouseup':
          this.showDescription(this.element);
          this.lastEvent = undefined;
          break;
        case 'mousedown':
          this.lastEvent = event;
          break;
        default:
      }
    } return;
  }

  /** Calculations of the difference btw mouse initial event and last event (drag gap) */
  handleElementDragging(event: any): void {
    if (!this.lastEvent || !this.element || !this.chart?.options?.plugins?.annotation?.annotations) {
      return;
    }
    /** if chart is updating, return */
    if (this.isUpdating) return;
    this.isUpdating = true;

    const coordinatesOfMove: Array<any> = this.getCoordinates(event.x, event.y);
    const coordinatesOfLastMove: Array<any> = this.getCoordinates(this.lastEvent.x, this.lastEvent.y);

    const moveX = coordinatesOfMove[0] - coordinatesOfLastMove[0];
    const moveY = coordinatesOfMove[1] - coordinatesOfLastMove[1];

    this.dragAndMove(moveX, moveY);
 
    this.isUpdating = false;
    this.lastEvent = event;

  }


  /** Calculations to drag shapes and move lines with them */
  dragAndMove(moveX: any, moveY: any): void {

    const annotations = this.chart?.options?.plugins?.annotation?.annotations as AnnotationOptions<any>[] | undefined;
    let elementId: string;

    if (annotations) {
      elementId = this.element.$context.id;

      if (elementId === 'box1' || elementId === 'ellipse1' || elementId === 'ellipse2' || elementId === 'ellipse3') {
        const annotation = annotations.find((a) => a.id === elementId);
        const width: number = this.getAnnotationWidthAndHeight(annotation)[0];  //gets element width
        const height: number = this.getAnnotationWidthAndHeight(annotation)[1]; //gets element height

        if (annotation) {
          this.getNewCoordinatesOfAnnotation(annotation, moveX, moveY);
          this.stopAnnotationAtCanvasBorder(annotations, width, height, elementId);
          this.moveSubAnnotationWithAnnotation(annotations, elementId);
        }
        this.moveLineWithAnnotations(annotations, elementId);
        this.chart.update();
        this.isUpdating = false;
      }
    }
  }

  /** Convert pixels to coordinates in order to get the right values to move the shapes */
  getCoordinates(pixelX: number, pixelY: number): Array<any> {
    let valueX;
    let valueY;
    for (let scaleName in this.chart?.scales) {
      let scale = this.chart?.scales[scaleName];
      if (scale.isHorizontal()) {
        valueX = scale.getValueForPixel(pixelX);
      } else {
        valueY = scale.getValueForPixel(pixelY);
      }
    }
    return [valueX, valueY];
  }


  getAnnotationWidthAndHeight(annotation: any): Array<number> {
    const width: number = Math.abs(annotation.xMin - annotation.xMax);
    const height: number = Math.abs(annotation.yMin - annotation.yMax);
    return [width, height];
  }

  /** Ccalculation of the shape's new coordinates while dragging the shape */
  getNewCoordinatesOfAnnotation(annotation: any, moveX: any, moveY: any) {
    annotation.xMin += moveX;
    annotation.xMax += moveX;
    annotation.yMin += moveY;
    annotation.yMax += moveY;
  }

  /** Logic which checks that no annotations escape from the canvas */
  stopAnnotationAtCanvasBorder(annotations: AnnotationOptions<any>[], elementWidth: number, elementHeight: number, annotationId: string) {
    this.rightBorder = this.chartMaxX - elementWidth;
    this.leftBorder = this.chartMaxY - elementHeight;
    const annotation = annotations.find((a) => a.id === annotationId);
    if (annotation.xMin <= 0) {
      annotation.xMin = 0;
      annotation.xMax = annotation.xMin + elementWidth;
    }

    if (annotation.xMin >= this.rightBorder) {
      annotation.xMin = this.rightBorder;
      annotation.xMax = annotation.xMin + elementWidth;
    }

    if (annotation.yMin <= 0) {
      annotation.yMin = 0;
      annotation.yMax = annotation.yMin + elementHeight;

    }

    if (annotation.yMin >= this.leftBorder) {
      annotation.yMin = this.leftBorder;
      annotation.yMax = annotation.yMin + elementHeight;
    }
  }

  /** Logic to move subannotations (i.e. descr. button, descr.label) with an annotation */
  moveSubAnnotationWithAnnotation(annotations: AnnotationOptions<any>[], shapeId: any) {

    /** id of the subAnnotation to move */
    const subAnnotationsToMove = this.findSubAnnotationsToMove(shapeId);

    for (let i = 0; i < subAnnotationsToMove.length; i++) {
      /** find subAnnotation to move with its connected annotation that is dragged */
      const subAnnotation = annotations.find((a) => a.id === subAnnotationsToMove[i]);
      const connectedAnnotation = annotations.find((a) => a.id === shapeId);
      const length = subAnnotation.width;
      const height = subAnnotation.height;

      if (subAnnotation.type === 'label') {
        subAnnotation.xValue = connectedAnnotation.xMax;
        subAnnotation.yValue = connectedAnnotation.yMax + 5;
      }
      if (subAnnotation.type === 'box') {
        subAnnotation.xMax = connectedAnnotation.xMax - 1;
        subAnnotation.xMin = connectedAnnotation.xMax - 5;
        subAnnotation.yMax = connectedAnnotation.yMax - 2;
        subAnnotation.yMin = connectedAnnotation.yMax - 8;

      }
    }
  }


  /** Logic to move lines when an annotation is dragged */
  moveLineWithAnnotations(annotations: AnnotationOptions<any>[], shapeId: any) {

    const linesToMove = this.findLinesToMove(shapeId);

    for (let i = 0; i < linesToMove.length; i++) {
      const lineEndToMove = this.findLineEndToMove(shapeId, linesToMove[i]);

      /** lineannotation to be moved with its connected annotation when it's dragged */
      const lineAnnotation = annotations.find((a) => a.id === linesToMove[i]);
      const connectedAnnotation = annotations.find((a) => a.id === shapeId);

      if (lineAnnotation) {
        if (lineEndToMove === 'firstSide') {
          lineAnnotation.xMin = connectedAnnotation.xMax;
          lineAnnotation.yMin = connectedAnnotation.yMin + (connectedAnnotation.yMax - connectedAnnotation.yMin) / 2;
        }
        if (lineEndToMove === 'endSide') {

          lineAnnotation.xMax = connectedAnnotation.xMin;
          lineAnnotation.yMax = connectedAnnotation.yMin + (connectedAnnotation.yMax - connectedAnnotation.yMin) / 2;

        }
      }
      else console.log("No such line annotation found");
    }

  }

  /** Subannotations that should move with each shape when the shape is dragged */
  findSubAnnotationsToMove(shapeId: any): Array<string> {
    switch (shapeId) {
      case 'box1': return ['descriptionButton', 'descriptionLabel'];
    }
    return [];
  }


  /** Lines that should move with each shape while the shape is dragged */
  findLinesToMove(shapeId: any): Array<string> {
    switch (shapeId) {
      case 'box1': return ['line1'];
      case 'ellipse1': return ['line1', 'line2', 'line3'];
      case 'ellipse2': return ['line2'];
      case 'ellipse3': return ['line3'];
    }
    return [];
  }

  /** mappings which end of the each line to move */
  findLineEndToMove(shapeId: any, lineId: any): string {

    switch (shapeId) {
      case 'box1':
        if (lineId === 'line1') return 'firstSide';
        return '';

      case 'ellipse1':
        if (lineId === 'line1') return 'endSide';
        if (lineId === 'line2' || lineId === 'line3') return 'firstSide';
        return '';

      case 'ellipse2':
      case 'ellipse3':
        if (lineId === 'line2' || lineId === 'line3') return 'endSide';
    }
    return '';
  }
}
