
import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FiledataService } from 'src/app/services/filedata.service';
import * as XLSX from 'xlsx';

export interface iData {
  dataElement: string;
  description: string;
  dataDictionaryTerm: string;
  businessDictionaryTerm1: string;
  businessDictionaryTerm2: string;
}

@Component({
  selector: 'app-file-upload-xlsx',
  templateUrl: './file-upload-xlsx.component.html',
  styleUrls: ['./file-upload-xlsx.component.css']
})
export class FileUploadXlsxComponent {

  data: any;
  storedData: any[] = [];
  errorMsg: string = '';
  successMsg: string = '';
  dataObject!: iData;
  dataObjects: iData[] = [];

  constructor(private dataService: DataService, private filedataService: FiledataService) { }

  ngOnInit() {
    this.getDataFromDatabase();
    this.removeDataFromLocalStorage();
  }

  getDataFromDatabase() {
    this.filedataService.getAllFilesData().subscribe({
      next: values => this.storedData = values,
      error: err => console.error(err),
    });
  }

  removeDataFromLocalStorage() {
    if (localStorage.getItem('dataRow') !== null) {
      localStorage.removeItem('dataRow');
    }
  }


  //drag-and-drop, from directives
  onFileDropped($event: any) {
    this.errorMsg = '';
    this.successMsg = '';
    if ($event === null) {
      this.errorMsg = "Korraga saab üles laadida vaid ühe faili!";
      return;
    }
    this.getFileContent($event);
  }

  //upload from filebrowser
  getFilesToUpload(target: any) {
    this.getFileContent(target.files);
    target.value = null;
  }

  //get content of file
  getFileContent(files: Array<any>) {
    this.readFileContent(files[0]);
  }

  //process file data
  readFileContent(file: any): any {
    this.errorMsg = '';
    this.successMsg = '';
    const fileReader = new FileReader();

    fileReader.onload = (e: any) => {
      const result: string = e.target.result;
      const workbook: XLSX.WorkBook = XLSX.read(result, { type: 'binary' });
      const worksheetName: string = workbook.SheetNames[0];
      const worksheet: XLSX.WorkSheet = workbook.Sheets[worksheetName];

      //map fields names from Excel for the app:
      worksheet['A1'].w = "dataElement";
      worksheet['B1'].w = "description";
      worksheet['C1'].w = "dataDictionaryTerm";
      worksheet['D1'].w = "businessDictionaryTerm1";
      worksheet['E1'].w = "businessDictionaryTerm2";

      this.data = XLSX.utils.sheet_to_json(worksheet);

      //check if strValue is empty string, null, undefined 
      for (let i = 0; i < this.data.length; i++) {
        if (this.data[i].dataElement === undefined ||
          this.data[i].dataElement === null ||
          String(this.data[i].dataElement).trim() === '') {
          this.errorMsg = `Viga andmetes! \'Andmeelement\' reas ${i} puudub, andmeid ei saa lisada!`;
          return;
        }

        if (this.data[i].description === undefined ||
          this.data[i].description === null ||
          String(this.data[i].description).trim() === '') {
          this.errorMsg = `Viga andmetes! \'Kirjeldus\' reas ${i + 1} puudub, andmeid ei saa lisada!`;
          return;
        }

        if (this.data[i].dataDictionaryTerm === undefined ||
          this.data[i].dataDictionaryTerm === null ||
          String(this.data[i].dataDictionaryTerm).trim() === '') {
          this.errorMsg = `Viga andmetes! \'Andmesõnastiku termin\' reas ${i + 2} puudub, andmeid ei saa lisada!`;
          return;
        }
      }

      this.errorMsg = '';

      //push data to the array
      for (let i = 0; i < this.data.length; i++) {
        this.storedData.push(this.data[i]);
      }

      this.successMsg = 'Faili üleslaadimine õnnestus!'

      // Save the data to the local storage 
      // localStorage['arrayData'] = JSON.stringify(this.storedData);

      //Save data to Java Spring BE:
      this.mapDataForBackend(this.data);
      this.sendDataRecordsToBackend(this.dataObjects);

    };

    fileReader.readAsBinaryString(file);
    this.successMsg = '';
  }

  //send data of a row to the chart (reads from webpage):
  sendRowData(value: any): void {
    const array: string[] = new Array;
    array.push(value.dataElement);
    array.push(value.description);
    array.push(value.dataDictionaryTerm);
    array.push(value.businessDictionaryTerm1);
    array.push(value.businessDictionaryTerm2);
    this.dataService.setDataArray(array);
  }

  mapDataForBackend(storedData: any): void {
    for (let i = 0; i < storedData.length; i++) {

      this.dataObject = {
        dataElement: '',
        description: '',
        dataDictionaryTerm: '',
        businessDictionaryTerm1: '',
        businessDictionaryTerm2: '',
      };

      this.dataObjects[i] = storedData[i];
    }
  }

  //send one row to backend
  sendDataToBackend(dataObject: iData): void {
    this.filedataService.addData(dataObject as iData).subscribe({
      error: (err) => console.log(err)
    });
  }

  //send multiple rows to backend
  sendDataRecordsToBackend(dataObjects: iData[]) {
    this.filedataService.addDataRecords(dataObjects as iData[]).subscribe({
      error: (err) => console.log(err)
    });
  }
}

